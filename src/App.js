import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import './App.css';
import Header from './components/Header'
import Body from './components/Body'
import Privacy from './components/Privacy'
import Terms from "./components/Terms";

const margin = {
    margin: "1.5rem"
};

function App() {
    return (
        <Router>
            <div style={margin} className="App">
                <header className="App-header">
                    <Header/>
                </header>
                <div style={margin}>
                    <Route exact path="/" component={Body} />
                    <Route path="/privacy" component={Privacy} />
                    <Route path="/terms" component={Terms} />
                </div>
            </div>
        </Router>
    );
}

export default App;
