import React, {Component} from 'react';
import {Navbar, Button, Row, Col, Container} from 'react-bootstrap';


class Header extends Component {
    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <Navbar.Brand href="/"><span className="logo">BotName</span> - Your personal bot</Navbar.Brand>
                    </Col>
                    <Col xs={3} lg={6} md={5}/>
                    <Col>
                    <Button variant="outline-primary" >Sign Up</Button>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Header;