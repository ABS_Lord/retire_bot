import React, {Component} from 'react';
import {Navbar, Image, Row, Col, Container} from 'react-bootstrap';
import Phone from './Phone'
import Footer from './Footer'
import apple from '../apple-badge.png'
import google from '../google-play-badge (1).png'

const hStyle = {
    color: "#3a47a1",
    fontSize: "4.5vw"
};

class Body extends Component {
    render() {
        return (
            <Container fluid>
                <Row>
                    <Col className="d-block d-sm-block d-md-none" xs={12}>
                        <h2 style={{color: "#3a47a1",fontSize: "10vw", textAlign: "center"}}>Get Better Everyday</h2>
                    </Col>
                    <Col md={6}>
                        <Phone/>
                    </Col>
                    <Col style={{marginTop: "3rem"}}>
                        <Row>
                            <Col className="d-none d-lg-block d-md-block"  md={1} lg={1} style={{textAlign: "center", margin: "0.8rem"}}><h2 style={hStyle}>Get Better Everyday</h2></Col>
                            <Col xs={1} md={10}></Col>
                        </Row>
                        <Row style={{padding: "3rem 0"}}>
                            <Col md={4} xs={6} style={{margin: "auto 0"}}>
                                <a href="https://apple.com"><Image style={{width: "92%"}} src={apple} fluid /></a>
                            </Col>
                            <Col md={4} xs={6} style={{margin: "auto 0"}}>
                                <a href="https://google.com"><Image src={google} fluid /></a>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col md={5}/>
                    <Col>
                    <Footer />
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Body;