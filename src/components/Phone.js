import React, {Component} from 'react';
import {Image, Container, Row, Col} from 'react-bootstrap';
import phone from "../new_phone.png"


class Phone extends Component {
    render() {
        return (
            <Container style={{marginTop: '5%'}}>
                <Row>
                    <Col md={2} />
                    <Col md={8}>
                        <Image src={phone} fluid />
                    </Col>
                    <Col md={2} />
                </Row>
            </Container>
        );
    }
}

export default Phone;