import React, {Component} from 'react';
import {Navbar, Button, Row, Col, Container} from 'react-bootstrap';

const aStyle = {
    fontSize: "0.7em",
    textAlign: "right"
};

class Footer extends Component {
    render() {
        return (
            <Container id="footer">
                <Row>
                    <Col style={{textAlign: "center"}} lg={2} md={3} xs={12}>
                        <a href={'#'} style={aStyle}>Contact us</a>
                    </Col>
                    <Col style={{textAlign: "center"}} lg={2} md={3} xs={12}>
                        <a href={'/privacy'} style={aStyle}>Privacy Policy</a>
                    </Col>
                    <Col style={{textAlign: "center"}} lg={3} md={4} xs={12}>
                        <a  href={'/terms'} style={aStyle}>Terms & Conditions</a>
                    </Col>
                    <Col lg={5} md={6} xs={12} style={{textAlign:"center"}} className="footer-copyright text-center">
                        <a style={aStyle}>©BotName ‑ Your Personal Bot</a>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Footer;